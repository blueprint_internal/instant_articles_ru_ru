
function Three_phones(resources)
{
	Three_phones.resources = resources;
}
Three_phones.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(950, 400, Phaser.CANVAS, 'three_phones', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,true);
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 950;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('item_1',Three_phones.resources.item_1);
        this.game.load.image('item_2',Three_phones.resources.item_2);
        this.game.load.image('item_3',Three_phones.resources.item_3);
		
		
		this.game.created = false;
    	
    	
    	
  
    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747
    	
	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
			
			this.parent.laptop = this.game.make.sprite(0,0,'item_1');
			this.parent.laptop.anchor.set(0.5,0.5);
            this.parent.game.stage.backgroundColor = '#ffffff';
            this.parent.buildAnimation();
            this.game.created  = true;
	    }
	},
    
	buildAnimation: function()
	{
		var style = Three_phones.resources.textStyle_1;
        var style_2 = Three_phones.resources.textStyle_2;
        var style_3 = Three_phones.resources.textStyle_3;
        var style_4 = Three_phones.resources.textStyle_4;
        this.item_1= this.game.add.sprite(this.game.world.centerX-400,this.game.world.centerY-28,'item_1');
        this.item_1.anchor.set(0.5,0.5);
        this.item_1.scale.setTo(.6,.6);
        //
        this.item_2= this.game.add.sprite(this.game.world.centerX-78,this.game.world.centerY-28,'item_2');
        this.item_2.anchor.set(0.5,0.5);
        this.item_2.scale.setTo(.6,.6);
        //
        this.item_3= this.game.add.sprite(this.game.world.centerX+230,this.game.world.centerY-28,'item_3');
        this.item_3.anchor.set(0.5,0.5);
        this.item_3.scale.setTo(.6,.6);
        
        //
        var percentage_1 = this.game.add.text(this.game.world.centerX-238,this.game.world.centerY-170, Three_phones.resources.percentage_1, style_3);
        percentage_1.wordWrap = true;
        percentage_1.wordWrapWidth = 155;
        percentage_1.anchor.set(0.5,0.5);
        //
        var percentage_2 = this.game.add.text(this.game.world.centerX+75,this.game.world.centerY-170, Three_phones.resources.percentage_2, style_3);
        percentage_2.wordWrap = true;
        percentage_2.anchor.set(0.5,0.5);
        percentage_2.wordWrapWidth = 155;
        //
        var percentage_3 = this.game.add.text(this.game.world.centerX+383,this.game.world.centerY-170, Three_phones.resources.percentage_3, style_3);
        percentage_3.wordWrap = true;
        percentage_3.anchor.set(0.5,0.5);
        percentage_3.wordWrapWidth = 155;
        
        
        //
        var title_1 = this.game.add.text(this.game.world.centerX-238,this.game.world.centerY-115, Three_phones.resources.title_1, style_2);
        title_1.wordWrap = true;
        title_1.wordWrapWidth = 155;
        title_1.anchor.set(0.5,0.5);
        //
        var title_2 = this.game.add.text(this.game.world.centerX+75,this.game.world.centerY-100, Three_phones.resources.title_2, style_2);
        title_2.wordWrap = true;
        title_2.anchor.set(0.5,0.5);
        title_2.wordWrapWidth = 155;
        //
        var title_3 = this.game.add.text(this.game.world.centerX+383,this.game.world.centerY-115, Three_phones.resources.title_3, style_2);
        title_3.wordWrap = true;
        title_3.anchor.set(0.5,0.5);
        title_3.wordWrapWidth = 155;
        
        var citation  = this.game.add.text(80,this.game.height-10, Three_phones.resources.citation, style_4);
        citation.anchor.set(0.5,0.5);
        
        
        //
        var text_1 = this.game.add.text(this.game.world.centerX-238,this.game.world.centerY+40, Three_phones.resources.text_1, style);
        text_1.wordWrap = true;
        text_1.wordWrapWidth = 155;
        text_1.anchor.set(0.5,0.5);
        //
        var text_2 = this.game.add.text(this.game.world.centerX+75,this.game.world.centerY+40, Three_phones.resources.text_2, style);
        text_2.wordWrap = true;
        text_2.anchor.set(0.5,0.5);
        text_2.wordWrapWidth = 155;
        //
        var text_3 = this.game.add.text(this.game.world.centerX+383,this.game.world.centerY+40, Three_phones.resources.text_3, style);
        text_3.wordWrap = true;
        text_3.anchor.set(0.5,0.5);
        text_3.wordWrapWidth = 155;
        
        //animations
        
        this.item_1_An = this.game.add.tween(this.item_1).to( { alpha:1,y:this.item_1.y},500,Phaser.Easing.Quadratic.Out);
        this.item_1.y-=50;
        this.item_1.alpha = 0;
        //
        this.item_2_An = this.game.add.tween(this.item_2).to( { alpha:1,y:this.item_2.y},500,Phaser.Easing.Quadratic.Out);
        this.item_2.y-=50;
        this.item_2.alpha = 0;
        //
        //
        this.item_3_An = this.game.add.tween(this.item_3).to( { alpha:1,y:this.item_3.y},500,Phaser.Easing.Quadratic.Out);
        this.item_3.y-=50;
        this.item_3.alpha = 0;
        
        
        this.text_1_An = this.game.add.tween(text_1).to( { alpha:1,y:text_1.y},500,Phaser.Easing.Quadratic.Out);
        text_1.y+=50;
        text_1.alpha = 0;
        //
        this.text_2_An = this.game.add.tween(text_2).to( { alpha:1,y:text_2.y},500,Phaser.Easing.Quadratic.Out);
        text_2.y+=50;
        text_2.alpha = 0;
        //
        this.text_3_An = this.game.add.tween(text_3).to( { alpha:1,y:text_3.y},500,Phaser.Easing.Quadratic.Out);
        text_3.y+=50;
        text_3.alpha = 0;
        
        this.title_1_An = this.game.add.tween(title_1).to( { alpha:1,y:title_1.y},500,Phaser.Easing.Quadratic.Out);
        title_1.y-=50;
        title_1.alpha = 0;
        //
        this.title_2_An = this.game.add.tween(title_2).to( { alpha:1,y:title_2.y},500,Phaser.Easing.Quadratic.Out);
        title_2.y-=50;
        title_2.alpha = 0;
        //
        this.title_3_An = this.game.add.tween(title_3).to( { alpha:1,y:title_3.y},500,Phaser.Easing.Quadratic.Out);
        title_3.y-=50;
        title_3.alpha = 0;
        
        //
        this.percentage_1_An = this.game.add.tween(percentage_1).to( { alpha:1,y:percentage_1.y,tweenedPoints:Three_phones.resources.percentage_1},500,Phaser.Easing.Quadratic.Out);
        percentage_1.y-=50;
        percentage_1.alpha = 0;
        percentage_1.text = "0%";
        percentage_1.tweenedPoints_1 = 0;
        this.percentage_1_An.onUpdateCallback(function(){
             
			percentage_1.setText(Math.round(percentage_1.tweenedPoints)+"%");
		}, this);
        
        //
        this.percentage_2_An = this.game.add.tween(percentage_2).to( { alpha:1,y:percentage_2.y,tweenedPoints:Three_phones.resources.percentage_2},500,Phaser.Easing.Quadratic.Out);
        percentage_2.y-=50;
        percentage_2.alpha = 0;
        percentage_2.text = "0%";
        percentage_2.tweenedPoints_1 = 0;
        this.percentage_2_An.onUpdateCallback(function(){
             percentage_2.setText(Math.round(percentage_2.tweenedPoints)+"%");
		}, this);
        //
        this.percentage_3_An = this.game.add.tween(percentage_3).to( { alpha:1,y:percentage_3.y,tweenedPoints:Three_phones.resources.percentage_3},500,Phaser.Easing.Quadratic.Out);
        percentage_3.y-=50;
        percentage_3.alpha = 0;
        percentage_3.text = "0%";
        percentage_3.tweenedPoints_1 = 0;
        this.percentage_3_An.onUpdateCallback(function(){
             percentage_3.setText(Math.round(percentage_3.tweenedPoints)+"%");
		}, this);
        
       
        this.item_1_An.chain(this.percentage_1_An,this.title_1_An,this.text_1_An,this.item_2_An,this.percentage_2_An,this.title_2_An,this.text_2_An,this.item_3_An,this.percentage_3_An,this.title_3_An,this.text_3_An);
        this.item_1_An.start();
        
        
        
        
        
        
		
    },
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
//#4080FF
//course/en/animations/nielsen_audience/
/*
var _resources= 
{
    "textStyle_1":{"font":"15px freight-sans-pro","fill": "#4267B2","wordWrap": false,"wordWrapWidth":200,"align": "center","lineSpacing": -10 },
    "textStyle_2":{"font":"22px freight-sans-pro","fill": "#4267B2","wordWrap": false,"wordWrapWidth":200,"align": "center","lineSpacing": -10 },
    "textStyle_3":{"font":"35px freight-sans-pro","fill": "#4267B2","wordWrap": false,"wordWrapWidth":200,"align": "center","lineSpacing": -10 },
    "item_1":"/Three_phones/phone_1.png",
    "item_2":"/Three_phones/phone_2.png",
    "item_3":"/Three_phones/phone_3.png",
    "title_1":"More Clicks",
    "title_2":"Less likely to Abandon",
    "title_3":"More shares",
    "percentage_1":"20",
    "percentage_2":"70",
    "percentage_3":"30",
    "text_1":"As people see more Instant Articles in News Feed, they read 20% more Instant Articles than mobile web articles on average.",
    "text_2":"Once they click they're over 70% less likely to anbandon the article because they're not stuck waiting for it to load.",
    "text_3":"People share 30% more Instant Article than mobile web articles on average, ampliflying the reach of your Stories in News Feed."
}
var start = new Three_phones(_resources).init();
*/


