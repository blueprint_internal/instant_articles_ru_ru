
function Threesixtyvideo(resources)
{
	Threesixtyvideo.resources = resources;
	
}
Threesixtyvideo.prototype = {
	init: function()
	{

		this.game = new Phaser.Game(800, 500, Phaser.CANVAS, 'threesixtyvideo', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		//659 139
		
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 500;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('phone', Threesixtyvideo.resources.phone);
		//this.game.load.image('placement', Threesixtyvideo.resources.placement);

		
		for(var i = 0;i<2;i++)
		{
			cnt = i+1;
			this.game.load.image('icon_'+cnt, Threesixtyvideo.resources['icon_'+cnt]);
			this.game.load.image('line_'+cnt, Threesixtyvideo.resources['line_'+cnt]);
		}
		
		this.game.created = false;
		this.game.stage.backgroundColor = 'ffffff';

	},

	create: function(evt)
	{
		//to remove touch
		/*
		Phaser.Canvas.setTouchAction(this.game.canvas, "auto");
		this.game.input.touch.preventDefault = false;
		*/
		this.iconCoordinatesAr = [{x:80,y:175},{x:695,y:130},{x:695,y:400},{x:80,y:175},{x:695,y:130},{x:695,y:400}];
		this.textCoordinatesAr = [{x:150,y:195},{x:620,y:230},{x:695,y:400},{x:80,y:175},{x:695,y:130},{x:695,y:400}];
		this.lineCoordinatesAr = [{x:115,y:290},{x:370,y:290},{x:400,y:295},{x:80,y:175},{x:695,y:130},{x:695,y:400}];

		//this.parent.placement = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY,'placement');
		//this.parent.placement.anchor.set(0.5);

		if(this.game.created === false)
		{
			
			this.parent.phone = this.game.add.sprite(this.game.world.centerX -20,this.game.world.centerY,'phone');
			
			this.parent.phone.anchor.set(0.5);
			this.parent.iconAr = [];
			this.parent.textAr = [];
			this.parent.lineAr = [];
			this.parent.iconAnimAr = [];
			this.parent.textAnimAr = [];
			this.parent.lineAnimAr = [];
			var cnt = 0;
			this.parent.style = Threesixtyvideo.resources.textStyle_1;
			this.game.load.image('icon_'+cnt, Threesixtyvideo.resources[cnt+"_icon"]);
			for(var i = 0;i<2;i++)
			{
				cnt = i+1;
				var tempText = this.game.add.text(this.textCoordinatesAr[i].x,this.textCoordinatesAr[i].y, Threesixtyvideo.resources["icon_text_"+cnt],this.parent.style);
				tempText.id = cnt;
				tempText.inputEnabled = false;
				///tempText.events.onInputDown.add(this.parent.actionOnClick, this);
				///tempText.input.useHandCursor = true;
				///tempText.input._setHandCursor = true;
				this.parent.textAr.push(tempText);
				this.parent.textAr[i].alpha = 0;
				this.parent.textAr[i].anchor.set(0.5,0.5);

				 //var test = game.add.sprite(200, 200, 'mushroom');

				 var temp_button = this.game.add.sprite(this.iconCoordinatesAr[i].x,this.iconCoordinatesAr[i].y,'icon_'+cnt,this);

				//var temp_button = this.game.add.button(this.iconCoordinatesAr[i].x,this.iconCoordinatesAr[i].y,'icon_'+cnt,null,this);
				
				temp_button.smoothed = true;
				temp_button.id = cnt;
				this.parent.iconAr.push(temp_button);
				
				///temp_button.useHandCursor = false;
				this.parent.iconAr[i].alpha = 0;
				this.parent.iconAr[i].anchor.set(0.5,0.5);
								
				//this.parent.textAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr.push(this.game.add.sprite(this.lineCoordinatesAr[i].x,this.lineCoordinatesAr[i].y,'line_'+cnt));
				//this.parent.lineAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr[i].alpha = 0;
				
				this.parent.iconAnimAr.push(this.game.add.tween(this.parent.iconAr[i]).to({alpha:1,x:this.iconCoordinatesAr[i].x},100,Phaser.Easing.Quadratic.Out));
				
				this.parent.textAnimAr.push(this.game.add.tween(this.parent.textAr[i]).to({alpha:1},500,Phaser.Easing.Quadratic.Out));

				this.parent.lineAnimAr.push(this.game.add.tween(this.parent.lineAr[i]).to({alpha:1},1000,Phaser.Easing.Quadratic.Out));

			}

			this.game.created  = true;
			this.parent.iconAr[0].x-=100;
			this.parent.iconAr[1].x+=100;
			//this.parent.iconAr[2].x+=120;
			this.parent.buildAnimation();
			
		}
	},

	actionOnClick: function(evt){
		
		//this.parent.trigger("domPopup",{title:Threesixtyvideo.resources['icon_text_'+evt.id],body:Threesixtyvideo.resources['popupMessage_'+evt.id]});

	},
    
	buildAnimation: function()
	{
		
		this.iconAnimAr[0].chain(this.iconAnimAr[1],this.lineAnimAr[0],this.lineAnimAr[1],this.textAnimAr[0],this.textAnimAr[1]);
		
		this.iconAnimAr[0].start();
		
	},
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}



