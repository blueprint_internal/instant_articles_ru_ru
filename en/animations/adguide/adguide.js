
function Adguide(resources)
{
	Adguide.resources = resources;
	
}
Adguide.prototype = {
	init: function()
	{

		this.game = new Phaser.Game(800, 600, Phaser.CANVAS, 'adguide', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{
		//659 139
		
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 600;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('phone', Adguide.resources.phone);
		//this.game.load.image('placement', Adguide.resources.placement);

		
		for(var i = 0;i<11;i++)
		{
			cnt = i+1;
			this.game.load.image('icon_'+cnt, Adguide.resources['icon_1']);
			this.game.load.image('line_'+cnt, Adguide.resources['line_1']);
		}
		
		this.game.created = false;
		this.game.stage.backgroundColor = 'ffffff';

	},

	create: function(evt)
	{
		//to remove touch
		/*
		Phaser.Canvas.setTouchAction(this.game.canvas, "auto");
		this.game.input.touch.preventDefault = false;
		*/
		this.iconCoordinatesAr = [{x:80,y:175},{x:695,y:130},{x:695,y:400},{x:80,y:175},{x:695,y:130},{x:695,y:400},{x:695,y:400},{x:695,y:400},{x:695,y:400},{x:695,y:400},{x:695,y:400}];




		//TEXT
		this.textCoordinatesAr = [{x:15,y:15},{x:15,y:120},{x:15,y:210},{x:15,y:310},{x:15,y:490},{x:513,y:15},{x:513,y:80},{x:513,y:130},{x:513,y:325},{x:513,y:410},{x:513,y:510}];
		//LINES
		this.lineCoordinatesAr = [{x:250,y:15},{x:250,y:120},{x:-200,y:-200},{x:250,y:310},{x:250,y:490},{x:493,y:15},{x:493,y:80},{x:493,y:130},{x:493,y:325},{x:493,y:410},{x:493,y:510}];

		//this.parent.placement = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY,'placement');
		//this.parent.placement.anchor.set(0.5);

		if(this.game.created === false)
		{
			
			this.parent.phone = this.game.add.sprite(this.game.world.centerX -20,this.game.world.centerY,'phone');
			
			this.parent.phone.anchor.set(0.5);
			this.parent.iconAr = [];
			this.parent.textAr = [];
			this.parent.lineAr = [];
			this.parent.iconAnimAr = [];
			this.parent.textAnimAr = [];
			this.parent.lineAnimAr = [];
			var cnt = 0;
			this.parent.style = Adguide.resources.textStyle_1;

			this.game.load.image('icon_'+cnt, Adguide.resources[cnt+"_icon"]);
			for(var i = 0;i<11;i++)
			{
				cnt = i+1;
				var tempText = this.game.add.text(this.textCoordinatesAr[i].x,this.textCoordinatesAr[i].y, Adguide.resources["icon_text_"+cnt],this.parent.style);
				tempText.id = cnt;
				tempText.inputEnabled = false;
				tempText.lineSpacing = -7;
				
				this.parent.textAr.push(tempText);
				this.parent.textAr[i].alpha = 0;
				this.parent.textAr[i].anchor.set(0,0);

				 var temp_button = this.game.add.sprite(this.iconCoordinatesAr[i].x,this.iconCoordinatesAr[i].y,'icon_'+cnt,this);

				temp_button.smoothed = true;
				temp_button.id = cnt;
				this.parent.iconAr.push(temp_button);
				
				this.parent.iconAr[i].alpha = 0;
				this.parent.iconAr[i].anchor.set(0.5,0.5);
								
				//this.parent.textAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr.push(this.game.add.sprite(this.lineCoordinatesAr[i].x,this.lineCoordinatesAr[i].y,'line_'+cnt));
				//this.parent.lineAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr[i].alpha = 0;
				
				this.parent.iconAnimAr.push(this.game.add.tween(this.parent.iconAr[i]).to({alpha:1,x:this.iconCoordinatesAr[i].x},100,Phaser.Easing.Quadratic.Out));
				
				this.parent.textAnimAr.push(this.game.add.tween(this.parent.textAr[i]).to({alpha:1},10,Phaser.Easing.Quadratic.Out));

				this.parent.lineAnimAr.push(this.game.add.tween(this.parent.lineAr[i]).to({alpha:1},100,Phaser.Easing.Quadratic.Out));

			}

			this.game.created  = true;
			this.parent.iconAr[0].x-=100;
			this.parent.iconAr[1].x+=100;
			//this.parent.iconAr[2].x+=120;
			this.parent.buildAnimation();
			
		}
	},

	actionOnClick: function(evt){
		
		//this.parent.trigger("domPopup",{title:Adguide.resources['icon_text_'+evt.id],body:Adguide.resources['popupMessage_'+evt.id]});

	},
    
	buildAnimation: function()
	{
		
		this.iconAnimAr[0].chain(this.textAnimAr[0],this.lineAnimAr[0],this.textAnimAr[1],this.lineAnimAr[1],this.textAnimAr[2],this.lineAnimAr[2],this.textAnimAr[3],this.lineAnimAr[3],this.textAnimAr[4],this.lineAnimAr[4],this.textAnimAr[5],this.lineAnimAr[5],this.textAnimAr[6],this.lineAnimAr[6],this.textAnimAr[7],this.lineAnimAr[7],this.textAnimAr[8],this.lineAnimAr[8],this.textAnimAr[9],this.lineAnimAr[9],this.textAnimAr[10],this.lineAnimAr[10]);
		
		this.iconAnimAr[0].start();
		
	},
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}



